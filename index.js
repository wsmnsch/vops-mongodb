const ObjectId = require('mongodb').ObjectId

module.exports = function ({
    connection,
    collection
}) {
    return {
        find: async function (doc) {
            return connection.collection(collection).findOne({_id: doc._id})
        },
        update: async function (criteria, update) {
            const result = await connection.collection(collection).updateOne(criteria, update)
            return result.result.nModified
        },
        updateOp: function (doc, op, attrs) {
            const criteria = { _id: doc._id }
            criteria['ops._id'] = new ObjectId(op._id)

            const update = {'$set': {}}

            Object.keys(attrs).forEach(function (key) {
                update['$set']['ops.$.' + key] = attrs[key]
            })

            return connection.collection(collection).updateOne(criteria, update)
        },
        initOp : function (ctx) {
            return {
                _id: ObjectId(),
            }
        },
        initCriteria : function (doc) {
            return {
                _id: doc._id
            }
        },
        initUpdate : function (op) {
            return {
                $push: {
                    'ops': op
                }
            }
        },
    }
}